MODULES += \
	app/system \
	app/shell \
	app/leds \
	app/buttons \
	app/eeprom \
	app/flash \
	app/adc \
	app/pwm \
	app/sensors \
	lib/version \
	lib/log \
	lib/cmd

ifeq ($(USE_DEVICE_AS5048A),)
MODULES += \
	app/encoder
endif


include project/target/izirunf0.mk

MODULE_DEPS += 	dev/memory/24cwxx \
				dev/memory/w25xx0cl \
				dev/temp_hum/am2320 \

ifeq ($(USE_DEVICE_AS5048A),)
MODULE_DEPS += 	\
	dev/motor/encoder/as5048a
endif

EXTRA_BUILDRULES += $(DEVICES_LK)/dev/memory/24cwxx/rules.mk \
					$(DEVICES_LK)/dev/memory/w25xx0cl/rules.mk \
					$(DEVICES_LK)/dev/temp_hum/am2320/rules.mk \
					$(DEVICES_LK)/dev/motor/encoder/as5048a/rules.mk