#include <app.h>
#include "stm32f0xx_hal.h"
#include <target/gpioconfig.h>
#include <kernel/thread.h>
#include "encoder.h"
#include <as5048a.h>

#define LOG_MODULE_NAME "ENCODER"
#define DEBUG (LOG_FATAL | LOG_ERROR | LOG_WARNING | LOG_INFO) //  | LOG_DEBUG)
#include "log/log.h"

motor_encoder_as5048a_device_t as5048a;

#if defined(WITH_LIB_CONSOLE)
#include <lib/console.h>

static int as5048a_console(int argc, const console_cmd_args *argv)
{
    int i = 0;
    int n = 0;
    if (argc == 3 && argv[1].u > 0)
    {
        n = argv[2].u;

        printf("===========AS5048A Magnitude and Angle============\r\n");
        for (i = 0; i < n; i++)
        {
            as5048a.get_value(&as5048a);
            printf("Magnitude: %d \n", as5048a.encoder.magnitude);
            printf("Angle: %f degree\n", (double)(as5048a.encoder.angle * 360) / 16384); // normalize 360° to 14 bit and convert the value in °
            thread_sleep(argv[1].u);
            printf("===================================================\r\n");
        }
    }
    else
    {
        printf("[Info] wrong format !\n");
    usage:
        printf("%s <interval(ms)> <repeat_nr>\n", argv[0].str);
        goto out;
    }
out:
    return 0;
}

#endif

STATIC_COMMAND_START
STATIC_COMMAND("as5048a", "read through SPI from AS5048A", (console_cmd_func)&as5048a_console)
STATIC_COMMAND_END(encoder);

static void encoder_init(const struct app_descriptor *app)
{
    print_info("Init ENCODER...\n");

    motor_encoder_as5048a_init(&as5048a, AS5048A_BUS, GPIO_SPI1_CS0);
}

APP_START(encoder)
    .init = encoder_init,
APP_END
