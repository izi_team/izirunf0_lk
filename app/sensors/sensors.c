#include <app.h>
#include "stm32f0xx_hal.h"
#include <dev/i2c.h>
#include <kernel/thread.h>
#include "sensors.h"
#ifdef USE_DEVICE_AM2320
#include <am2320.h>
#endif

#define LOG_MODULE_NAME "SENSORS"
#define DEBUG   (LOG_FATAL | LOG_ERROR | LOG_WARNING | LOG_INFO)//  | LOG_DEBUG)
#include "log/log.h"

#ifdef USE_DEVICE_AM2320
temp_hum_am2320_device_t am2320;
#endif

#if defined(WITH_LIB_CONSOLE)
#include <lib/console.h>

#ifdef USE_DEVICE_AM2320
static int am2320_console(int argc, const console_cmd_args *argv)
{
 int i = 0;
 int n = 0;
 if( argc == 3 && argv[1].u > 0)
  {
	n = argv[2].u;

	float temperature, humidity;
    printf("==========AM2320 Temperature and Humidity ==========\r\n");
    for (i = 0; i < n; i++)
	{
		if (am2320.read(&am2320, &humidity, &temperature)) {
			print_error("FAIL to communicate with am2320 !\n");
			goto out;
		}
  		printf("Temperature: %f C\n", temperature);
  		printf("Humidity: %f %%\n", humidity);
		thread_sleep(argv[1].u);
		printf("====================================================\r\n");

	}
  }
 else
  {
 	printf("[Info] wrong format !\n");
	usage:
		printf("%s <interval(ms)> <repeat_nr>\n", argv[0].str);
		goto out;
  }
out:
    return 0;
}
#endif

STATIC_COMMAND_START
#ifdef USE_DEVICE_AM2320
STATIC_COMMAND("am2320", "read through I2C from AM2320", (console_cmd_func)&am2320_console)
#endif
STATIC_COMMAND_END(sensors);

#endif

static void sensors_init(const struct app_descriptor *app)
{
    print_info("Init SENSORS...\n");

#ifdef USE_DEVICE_AM2320
    temp_hum_am2320_init(&am2320, AM2320_BUS, AM2320_I2C_ADDRESS);
#endif
}

APP_START(sensors)
    .init   = sensors_init,
APP_END