
#include <system.h>
#include <app.h>
#include <string.h>
#include <stdio.h>
#include <dev/gpio.h>
#include <target/gpioconfig.h>
#include <lk/err.h>

#include <platform/timer_pwm.h>
#include "pwm.h"

#define LOG_MODULE_NAME "PWM"
#define DEBUG   (LOG_FATAL | LOG_ERROR | LOG_WARNING | LOG_INFO)// | LOG_DEBUG)
#include "log/log.h"


#if defined(WITH_LIB_CONSOLE)
#include <lib/console.h>

static int pwm_console(int argc, const console_cmd_args *argv);

STATIC_COMMAND_START
STATIC_COMMAND("pwm", "generate pwm", (console_cmd_func)&pwm_console)
STATIC_COMMAND("channels", "visualise used channels of timerx", (console_cmd_func)&pwm_console)
STATIC_COMMAND("set frequency", "set pwm frequency", (console_cmd_func)&pwm_console)
STATIC_COMMAND("set resolution", "set timer resolution", (console_cmd_func)&pwm_console)
STATIC_COMMAND("set mode", "set pwm mode", (console_cmd_func)&pwm_console)
STATIC_COMMAND("set type", "set pwm type", (console_cmd_func)&pwm_console)
STATIC_COMMAND_END(pwm);

static int pwm_console(int argc, const console_cmd_args *argv)
{

    if ((argc < 2) || (argc > 4) ) {
usage:  // I have a problem here
        printf("wrong arguments !\n");
        printf("pwm:            <GPIO pin><duty cycle>\n");
        printf("channels:       <timer>\n");
        printf("set frequency:  <GPIO pin><frequency in kHz>\n");
        printf("set resolution: <GPIO pin><resolution in bit>\n");
        printf("set mode:       <GPIO pin><mode>\n");
        printf("set type:       <GPIO pin><type>\n");
        printf("argc=%d\n",argc);
        printf("argv=%ld\n",argv[1].u);
        printf("%s\n", argv[0].str);
        pwm(0,100);
        pwm(1,50);
        pwm(2,10);
        pwm(3,1);
        pwm(4,40);
        pwm(5,100);
        pwm(6,50);
        print_channel_used(3);
        print_channel_used(16);
        print_channel_used(17);
        print_channel_used(1);
        printf("done\n");
        goto out;
    }

    else if ((argc==2) && (strcmp(argv[1].str,"channels")==0)){
        int timer=argv[2].u;
        print_channel_used(timer);
        goto out;
    }

    else if( (argc == 3) && (strcmp(argv[1].str,"pwm")==0)){
        int pin= argv[2].u;
        int duty_cycle=argv[3].u;
        pwm(pin,duty_cycle);
        printf("pwm on pin\n");
        goto out;
    }
    else if ((argc == 4) && (strcmp(argv[1].str,"set")==0)){
        if( strcmp(argv[2].str,"frequency")==0){
            int pin=argv[3].u;
            uint8_t frequency=argv[4].u;
            set_frequency(pin,frequency);
            goto out;
        }
        else if( strcmp(argv[2].str,"resolution")==0){
            int pin=argv[3].u;
            int resolution=argv[4].u;
            set_resolution(pin,resolution);
            goto out;
        }

        else if( strcmp(argv[2].str,"mode")==0){
            int pin=argv[3].u;
            int mode=argv[4].u;
            set_pwm_mode(pin,mode);
            goto out;
        }
         else if( strcmp(argv[2].str,"type")==0){
            int pin=argv[3].u;
            int type=argv[4].u;
            set_pwm_type(pin,type);
            goto out;
        }
        else {
            printf("go to usage\n");
            goto usage;
            
        }

    }
    else{
        goto usage;
        printf("go to usage 2\n");
    }
        
out:
    return 0;
}


#endif

// function to set the corresponding pin to alternate function 
// enable the rcc to port
// set the alternate function mode in MODER

static void set_gpio_AFN(int pwm_pin){
    int timer=pwm_pins[pwm_pin].timer;
    int chan =pwm_pins[pwm_pin].channel;
    printf("%d:%d\n",timer,chan);

    switch(pwm_pin){
        case 0:
        //gpio_config(GPIO_PWM0,       GPIO_AFn(1));   // PB4 | TIM3 | channel 1 | AFN[1]
        RCC->AHBENR  |= (1U << 18);
        GPIOB->MODER |= (1U << 9);
        GPIOB->MODER &=~(1U << 8);
        GPIOB->AFR[0] |=(1U << 16);
        //pwm_init_chan(timer,chan);     
        break;

        case 1:
        //gpio_config(GPIO_PWM1,       GPIO_AFn(1));   // PB5 | TIM3 | channel 2 | AFN[1]
        RCC->AHBENR  |= (1U << 18);
        GPIOB->MODER |= (1U << 11);
        GPIOB->MODER &=~(1U << 10);
        GPIOB->AFR[0] |=(1U << 20);
        //pwm_init_chan(timer,chan); 
        break;
        
        case 2:
        //gpio_config(GPIO_PWM2,       GPIO_AFn(1));   // PB0 | TIM3 | channel 3 | AFN[1]
        RCC->AHBENR  |= (1U << 18);
        GPIOB->MODER |= (1U << 1);
        GPIOB->MODER &=~(1U << 0);
        GPIOB->AFR[0] |=(1U << 0);
         
        break;

        case 3:
        //gpio_config(GPIO_PWM3,       GPIO_AFn(1));   // PB1 | TIM3 | channel 4 | AFN[1]
        RCC->AHBENR  |= (1U << 18);
        GPIOB->MODER |= (1U << 3);
        GPIOB->MODER &=~(1U << 2);
        GPIOB->AFR[0] |=(1U << 4);
        
        break;

        case 4:
        //gpio_config(GPIO_PWM4,       GPIO_AFn(2));   // PB8 | TIM16 | channel 1 | AFN[2]
        RCC->AHBENR  |= (1U << 17);
        GPIOB->MODER |= (1U << 17);
        GPIOB->MODER &=~(1U << 16);
        GPIOB->AFR[1] |=(1U << 1);
        
        break;

        case 5:
        //gpio_config(GPIO_PWM5,       GPIO_AFn(2));   // PB9 | TIM17 | channel 1 | AFN[2]
        RCC->AHBENR  |= (1U << 18);
        GPIOB->MODER |= (1U << 19);
        GPIOB->MODER &=~(1U << 18);
        GPIOB->AFR[1] |=(1U << 5 );
        
        break;
        case 6:
        //gpio_config(GPIO_LED_GREEN,  GPIO_AFn(2));   // PA8 | TIM1  | channel 1 | AFN[2]
        RCC->AHBENR  |= (1U << 17);
        GPIOA->MODER |= (1U <<17);
        GPIOA->MODER &=~(1U << 16);
        GPIOA->AFR[1] |=(1U << 1);
        
        break;
    }
}

// function that display all channels used 
static void print_channel_used(int timer){
    int *channel_used=get_used_channels(timer);
    printf("channels used of timer %d\n",timer);
    for(int i=0;i<4;i++)
        printf("%d | ",channel_used[i]);
    printf("\n");
}


// function thatgenerate pwm signal on the pin given as argument
// the frequency and the resolution of pwmsignal is set as default :
// f          = 25khz
// resolution = 10 bits
// this configuration can be modified by the user 

static void pwm(int pin, int duty_cycle){
    int timer=pwm_pins[pin].timer;
    int chan =pwm_pins[pin].channel;
    set_gpio_AFN(pin);
    pwm_init_chan(timer,chan);
    set_duty_cycle(timer,chan,duty_cycle);
    pwm_start(timer);
    printf("done here\n");
}

// function to set the pwm' frequency dreven by a given pin
// note that changing the pwm frequency is changing the timer clock
// so all channels will generate pwmsignal withthe same frequency ( but with different duty cycles)

static void set_frequency(int pin, int f ){

    int timer=pwm_pins[pin].timer;
    set_pwm_frequency(timer,f);

}

// function to set the pwm' frequency resolution by a given pin


static void set_resolution(int pin, int resolution ){
    
    int timer=pwm_pins[pin].timer;
    set_timer_resolution(timer,resolution);
}

// this function set the pwm mode dreven by a given pin
// there's ywo pwmmodes:
// + mode 1: channelx is active as long as CNT<CCRx else inactive
// + mode 2: channelx is inactive as long as CNT<CCRx else active (mode 1 inverted)
// mode 1 is usally used
static void set_pwm_mode(int pin, int mode ){
    
    int timer=pwm_pins[pin].timer;
    int chan =pwm_pins[pin].channel;
    set_channel(timer,chan,mode);
}


// function to set pwm type
// type = 0 : Edge-aligned mode
// type = 1 : Center-aligned mode 1
// type = 2 : Center-aligned mode 2
// type = 3 : Center-aligned mode 3
// Edge-aligned is usually used
static void set_pwm_type(int pin, int type ){
    
    int timer=pwm_pins[pin].timer;
    set_type(timer,type);
}