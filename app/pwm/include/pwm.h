#include <platform/gpio.h>

typedef struct gpio_configuration {
    int timer ;           // timer 
    
    int channel;          // timer's channels

}gpio_configuration_t;

const gpio_configuration_t pwm_pins[] = {

#ifdef GPIO_PWM0
    [0] = {
        .timer    = 3,
        .channel  = 1,
    },
#endif
#ifdef GPIO_PWM1
    [1] = {
        .timer    = 3,
        .channel  = 2,
    },
#endif
#ifdef GPIO_PWM2
    [2] = {
        .timer    = 3,
        .channel  = 3,
    },
#endif
#ifdef GPIO_PWM3
    [3] = {
        .timer    = 3,
        .channel  = 4,
    },
#endif
#ifdef GPIO_PWM4
    [4] = {
        .timer    = 16,
        .channel  = 1,
    },
#endif
#ifdef GPIO_PWM5
    [5] = {
        .timer    = 17,
        .channel  = 1,
    },
#endif
#ifdef GPIO_LED_GREEN
    [6] = {
        .timer    = 1,
        .channel  = 1,
    },
#endif
};


static void set_gpio_AFN(int pwm_pin);
static void print_channel_used(int timer);
static void pwm(int pin, int duty_cycle);
static void set_frequency(int pin, int f );
static void set_resolution(int pin, int resolution );
static void set_pwm_mode(int pin, int mode );
static void set_pwm_type(int pin, int type );