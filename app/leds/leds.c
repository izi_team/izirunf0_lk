/*
 * Copyright (c) 2019-2020 IZITRON (Mihail Cherciu)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <system.h>
#include <app.h>
#include <string.h>
#include <dev/gpio.h>
#include <target/gpioconfig.h>
#include <lk/err.h>
#include "leds.h"

#define LOG_MODULE_NAME "LEDS"
#define DEBUG   (LOG_FATAL | LOG_ERROR | LOG_WARNING | LOG_INFO)// | LOG_DEBUG)
#include "log/log.h"

#define EVENT(x) (led.cmd.what == (x))

led_device_t led;

#if defined(WITH_LIB_CONSOLE)
#include <lib/console.h>

static int led_console(int argc, const console_cmd_args *argv);

STATIC_COMMAND_START
STATIC_COMMAND("led", "drive leds", (console_cmd_func)&led_console)
STATIC_COMMAND_END(led);

static int led_console(int argc, const console_cmd_args *argv)
{
    int i;

    if (argc < 3) {
        printf("not enough arguments !\n");
        printf("%d",argc);
usage:
        printf("%s green <on/off/blink>\n", argv[0].str);
        goto out;
    }

    if (!strcmp(argv[1].str, "green")) {
        if (!strcmp(argv[2].str, "on")) {
            leds_cmd(LEDS_CMD_GREEN_ON);
        }
        else if (!strcmp(argv[2].str, "off")) {
            leds_cmd(LEDS_CMD_GREEN_OFF);
        }
        else if (!strcmp(argv[2].str, "blink")) {
            leds_cmd(LEDS_CMD_GREEN_BLINK);
        }
        else {
            goto usage;
        }
    }
    else {
        goto usage;
    }

out:
    return 0;
}

#endif

static void led_inform_sys_power_up(void) {
    int i;

    for (i=0; i<20; i++) {
        gpio_set(GPIO_LED_GREEN,  1);
        thread_sleep(50);
        gpio_set(GPIO_LED_GREEN,  0);
        thread_sleep(50);
    }
}

static void set_led_off(void) {
    gpio_set(GPIO_LED_GREEN, 0);
}

static void set_time_blink(int freq_ms, int keep_on_ms, int color) {
    led.timing.ms_blink  = keep_on_ms;
    led.timing.ms_freq   = freq_ms;
    led.timing.color     = color;
}

static void reset_time_blink(void) {
    set_led_off();
    set_time_blink(0, 0, 0);
}

static void set_color_green(bool on_off) {
    if (on_off)
        gpio_set(GPIO_LED_GREEN,  1);
    else
        gpio_set(GPIO_LED_GREEN,  0);
}

static enum handler_return led_off_timer_callback(struct timer *timer, lk_time_t now, void *arg)
{
    led_timer_t *t = (led_timer_t*) arg;

    if (t->ms_blink == 0) return INT_NO_RESCHEDULE;

    set_led_off();

    return INT_NO_RESCHEDULE;
}

static enum handler_return led_on_timer_callback(struct timer *timer, lk_time_t now, void *arg)
{
    led_timer_t *t = (led_timer_t*) arg;

    if (t->ms_freq == 0) return INT_NO_RESCHEDULE;

    if (t->color == LED_GREEN)   led.g(true);

    timer_set_oneshot(&t->timer_off, t->ms_blink, &led_off_timer_callback, arg);
    timer_set_periodic(&t->timer_on, t->ms_freq,  &led_on_timer_callback,  arg);
    return INT_NO_RESCHEDULE;
}

static void leds_init(const struct app_descriptor *app)
{
    print_debug("Init LEDs...\n");

    /* Init timers */
    timer_initialize(&led.timing.timer_on);
    timer_initialize(&led.timing.timer_off);

    /* Init led event */
    event_init(&led.event, false, EVENT_FLAG_AUTOUNSIGNAL);

    led.timing.ms_blink = 0;
    led.timing.ms_freq  = 0;
    led.g               = &set_color_green;
    led.set             = &set_time_blink;
    led.reset           = &reset_time_blink;

    led_inform_sys_power_up();

    timer_set_periodic(&led.timing.timer_on,  led.timing.ms_freq,  &led_on_timer_callback, (void *)&led.timing);
}

static void leds_entry(const struct app_descriptor *app, void *args)
{
    int ret = 0;

    thread_set_priority(HIGH_PRIORITY+2);

    while (event_wait(&led.event) == NO_ERROR) {
        print_debug("LEDs event: %d\n", led.cmd.what);

        /* GREEN */
        if (EVENT(LEDS_CMD_GREEN_OFF)) {
            print_debug("LEDs event: LEDS_CMD_GREEN_OFF\n");
            led.reset();
            led.g(false);
        }
        else if (EVENT(LEDS_CMD_GREEN_ON)) {
            print_debug("LEDs event: LEDS_CMD_GREEN_ON\n");
            led.reset();
            led.g(true);
        }
        else if (EVENT(LEDS_CMD_GREEN_BLINK)) {
            print_debug("LEDs event: LEDS_CMD_GREEN_BLINK\n");
            led.set(1000, 500, LED_GREEN);
        }
        else if (EVENT(LEDS_CMD_GREEN_BLINK_SLOW)) {
            print_debug("LEDs event: LEDS_CMD_GREEN_BLINK_SLOW\n");
            led.set(2000, 250, LED_GREEN);
        }
    }
}

APP_START(leds)
    .init   = leds_init,
    .entry  = leds_entry,
APP_END

int leds_cmd(leds_actions_t action)
{
    send_cmd(action, &led.cmd, &led.event);
    return 0;
}
