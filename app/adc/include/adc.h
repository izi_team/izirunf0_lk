#ifndef ADC_H_
#define ADC_H_

#define VREFINT_CAL_ADDR    ((uint16_t*) (0x1FFFF7BAU))
#define VREFINT_CAL_VREF    (3.3)

typedef enum {
    IZIRUNF0_ADC0 = 1,       //IZIRUNF0_ADC0 is mapping to ADC1
    IZIRUNF0_ADC1 = 2,       //IZIRUNF0_ADC1 is mapping to ADC2
    IZIRUNF0_VREF = 17       //Internal adc vref
} adc_ch_t;

#define ADC_MIN             0U
#define ADC_MAX             4095U

#define ADC_MIN_VOLTAGE     0
#define ADC_MAX_VOLTAGE     3.4

#endif  // ADC_H_
