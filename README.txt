How to build and flash (linux):
1.  Install cross compiler:
    Binutils:
        (Ubuntu)
        sudo apt-get install binutils-arm-none-eabi make
        
        (Fedora)
        sudo dnf install arm-none-eabi-binutils-cs

    GCC Arm cross-compiler
        (Ubuntu)
        sudo apt-get install gcc-arm-none-eabi
        
        (Fedora)
        sudo dnf install arm-none-eabi-gcc-cs
        sudo dnf install arm-none-eabi-newlib


    GDB debugger:
        (Ubuntu)
        sudo apt-get install gdb-multiarch
        
        (Fedora)
        sudo dnf install gdb
        

2. Clone LK form IZITRON repo
    git clone https://bitbucket.org/izi_team/lk.git -b izitron
    
3. Clone devices_lk from IZITRON repo
    git clone https://bitbucket.org/izi_team/devices_lk.git

4. Clone izirunf0 from IZITRON repo
    git clone https://bitbucket.org/izi_team/izirunf0_lk.git
    cd izirunf0_lk
    make

5. Flash lk
    Install tools
        sudo apt-get install gcc build-essential
        sudo apt-get install git make cmake libusb-1.0-0-dev
        
    Using ST-LINK
        Install stlink
            git clone https://github.com/stlink-org/stlink -b master
            cd stlink
            cmake .
            make
            #install binaries:
            sudo cp bin/st-* /usr/local/bin
            #install udev rules
            sudo cp config/udev/rules.d/49-stlinkv* /etc/udev/rules.d/
            #and restart udev
            sudo udevadm control --reload

        Flash lk.bin
            st-flash write build-izirunf0_izigoboard/lk.bin 0x8000000

    Using UART
        Install stm32flash-code
            git clone https://git.code.sf.net/p/stm32flash/code stm32flash-code
            cd stm32flash-code
            make
            sudo make install
            sudo gpasswd --add ${USER} dialout

        Flash lk.bin
            stm32flash -w build-izirunf0_izigoboard/lk.bin -v  /dev/ttyUSBx
