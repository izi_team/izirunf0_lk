LOCAL_DIR := izirunf0_lk
LKMAKEROOT := ..
LKROOT := lk
DEVICES_LK ?= devices_lk
LKINC := $(LOCAL_DIR) $(DEVICES_LK)
DEFAULT_PROJECT ?= izirunf0_izigoboard
BUILDROOT ?= $(LOCAL_DIR)
TOOLCHAIN_PREFIX = arm-none-eabi-
