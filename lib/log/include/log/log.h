/****************************************************************************************
 * log.h
 *
 *  Created on: May 19, 2018
 *      Author: Mihail CHERCIU (mcherciu@gmail.com)
 ****************************************************************************************/

#ifndef INC_LOG_H_
#define INC_LOG_H_

#ifndef LOG_MODULE_NAME
    #define LOG_MODULE_NAME "UNKNOWN"
#endif

#ifndef DEBUG
    #define DEBUG                       0b00000000
#endif

/* Log level */
#define LOG_FATAL                       0b00000001
#define LOG_ERROR                       0b00000010
#define LOG_WARNING                     0b00000100
#define LOG_INFO                        0b00001000
#define LOG_DEBUG                       0b00010000

void print_dbg( uint8_t         log_level,
                const char *    module_name,
                const char *    file_name,
                uint16_t        line_nr,
                const char *    format, ...);

#if (DEBUG & LOG_FATAL)
    #define print_fatal(...)                print_dbg(LOG_FATAL, LOG_MODULE_NAME, __FILE__ , __LINE__ , __VA_ARGS__)
#else
    #define print_fatal(...)
#endif

#if (DEBUG & LOG_ERROR)
    #define print_error(...)                print_dbg(LOG_ERROR, LOG_MODULE_NAME, __FILE__ , __LINE__ , __VA_ARGS__)
#else
    #define print_error(...)
#endif

#if (DEBUG & LOG_WARNING)
    #define print_warning(...)              print_dbg(LOG_WARNING, LOG_MODULE_NAME, __FILE__ , __LINE__ , __VA_ARGS__)
#else
    #define print_warning(...)
#endif

#if (DEBUG & LOG_INFO)
    #define print_info(...)                 print_dbg(LOG_INFO, LOG_MODULE_NAME, __FILE__ , __LINE__ , __VA_ARGS__)
#else
    #define print_info(...)
#endif

#if (DEBUG & LOG_DEBUG)
    #define print_debug(...)                print_dbg(LOG_DEBUG, LOG_MODULE_NAME, __FILE__ , __LINE__ , __VA_ARGS__)
#else
    #define print_debug(...)
#endif

#define ERROR_CHECK(ERROR_CODE)                 \
    do                                          \
    {                                           \
        const int32_t ERR_CODE = (ERROR_CODE);  \
        if (ERR_CODE != NO_ERROR)               \
        {                                       \
            print_error("Code: %d\n",ERR_CODE); \
        }                                       \
    }while(0)                                   \

#endif /* INC_LOG_H_ */
