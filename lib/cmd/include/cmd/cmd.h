/****************************************************************************************
 * cmd.h
 *
 *  Created on: May 19, 2018
 *      Author: Mihail CHERCIU (mcherciu@gmail.com)
 ****************************************************************************************/

#ifndef CMD_H_
#define CMD_H_

#include <kernel/mutex.h>
#include <kernel/event.h>

typedef struct {
    int what;
} cmd_t;

int send_cmd(int action, cmd_t *cmd, event_t *event);

#endif /* CMD_H_ */
