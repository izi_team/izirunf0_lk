/*
 * Copyright (c) 2012 Travis Geiselbrecht
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __TARGET_GPIOCONFIG_H
#define __TARGET_GPIOCONFIG_H

#include <platform/gpio.h>

/*-------------------LED-----------------------*/
#define GPIO_LED_GREEN      GPIO(GPIO_PORT_A, 8)
/*---------------------------------------------*/

/*------------------BUTTON---------------------*/
#define GPIO_BTN_USER       GPIO(GPIO_PORT_B, 2)
/*---------------------------------------------*/

/*-------------------ADC-----------------------*/
#define GPIO_ADC0           GPIO(GPIO_PORT_A, 1)
#define GPIO_ADC1           GPIO(GPIO_PORT_A, 2)
/*---------------------------------------------*/

/*-------------------PWM-----------------------*/
#define GPIO_PWM0           GPIO(GPIO_PORT_B, 4)
#define GPIO_PWM1           GPIO(GPIO_PORT_B, 5)
#define GPIO_PWM2           GPIO(GPIO_PORT_B, 0)
#define GPIO_PWM3           GPIO(GPIO_PORT_B, 1)
#define GPIO_PWM4           GPIO(GPIO_PORT_B, 8)
#define GPIO_PWM5           GPIO(GPIO_PORT_B, 9)
/*---------------------------------------------*/

/*-------------------IO------------------------*/
#define GPIO_IO0            GPIO(GPIO_PORT_A, 3)
#define GPIO_IO1            GPIO(GPIO_PORT_A, 4)
/*---------------------------------------------*/

/*-----------------SHARE-----------------------*/
/* UART1 for CONSOLE */
#define GPIO_UART1_TX       GPIO(GPIO_PORT_A, 9)
#define GPIO_UART1_RX       GPIO(GPIO_PORT_A, 10)

/* UART3 for CAN */
#define GPIO_UART3_TX       GPIO(GPIO_PORT_B, 10)
#define GPIO_UART3_RX       GPIO(GPIO_PORT_B, 11)

/* I2C1 */
#define GPIO_I2C1_CLK       GPIO(GPIO_PORT_B, 6)
#define GPIO_I2C1_SDA       GPIO(GPIO_PORT_B, 7)

/* I2C2 for EEPROM */
#define GPIO_I2C2_CLK       GPIO(GPIO_PORT_A, 11)
#define GPIO_I2C2_SDA       GPIO(GPIO_PORT_A, 12)

/* SPI1 */
#define GPIO_SPI1_SCK       GPIO(GPIO_PORT_A, 5)
#define GPIO_SPI1_MISO      GPIO(GPIO_PORT_A, 6)
#define GPIO_SPI1_MOSI      GPIO(GPIO_PORT_A, 7)
#define GPIO_SPI1_CS0       GPIO(GPIO_PORT_C, 13)
#define GPIO_SPI1_CS1       GPIO(GPIO_PORT_A, 15)
#define GPIO_SPI1_CS2       GPIO(GPIO_PORT_B, 3)

/* SPI2 for FLASH */
#define GPIO_SPI2_CS        GPIO(GPIO_PORT_B, 12)
#define GPIO_SPI2_SCK       GPIO(GPIO_PORT_B, 13)
#define GPIO_SPI2_MISO      GPIO(GPIO_PORT_B, 14)
#define GPIO_SPI2_MOSI      GPIO(GPIO_PORT_B, 15)
/*---------------------------------------------*/

#endif
